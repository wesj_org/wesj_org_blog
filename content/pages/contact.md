---
title: "Contacting Me"
date: 2021-02-25T21:53:40-05:00
draft: false
author: wesj
---

If you must:

Email: `wes` AT _this domain_

Please don't follow me on social media.