---
title: "What is this"
date: 2021-02-25T21:00:28-05:00
draft: false
author: wesj
---

This site is a landing page for the wesj.org domain and a blog-ish thing for any writing I feel like doing (which is almost none).
Mostly it serves as a dumping ground for old projects and things I'm interested in so they don't get forgotten.

See [Directory]({{< ref "/pages/directory.md" >}}) for more info on what's hosted here.

See [Projects]({{< ref "/pages/projects.md">}}) for Projects that I have (*cough* abandoned *cough*) worked on.

[Contact Info]({{< ref "/pages/contact.md" >}})