---
title: "Projects"
date: 2021-03-28T11:43:53-04:00
draft: false
author: wesj
---

My Github page is [here](https://github.com/Thing342).

My GitLab page is [here](https://gitlab.com/wesjordansemailaddress).

# Active Projects

* This site - Hugo static site landing page/blog-type thing
    - https://gitlab.com/wesj_org/wesj_org_blog
* Wesj K3s Argo - Reference Kubernetes Devops environment for this site and other apps
    - https://gitlab.com/wesj_org/wesj-k3s-argo
* Twitter Deleter - Deletes your old Tweets without selling your data
    - https://gitlab.com/wesj_org/twitter-deleter

# Less-active Projects

* My AdventOfCode 2020 solutions, written in Rust
    - https://gitlab.com/wesjordansemailaddress/adventofcode20

* YASA - A prototype scouting application for FIRST Robotics Competition using customizable HTML scoresheets.
    - https://gitlab.com/wesjordansemailaddress/yet-another-scouting-app

* PunBot - Haskell-based Slack Chat reader/poster bot
    - https://gitlab.com/wesj_org/punbot

# Dead Projects

* Let's go Eat Already - 2017 VTHacks Submission
    - https://github.com/Thing342/vthacks2017

* LayoutBasedScouting - Customizable Scouting Application using JSON scoresheets; Written for the 2015 FRC Season.
    - PC component: https://github.com/Thing342/FRCScoutingCompiler
    - Android component: https://github.com/Thing342/LayoutBasedFRCScouting