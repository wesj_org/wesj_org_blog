---
title: "Directory"
date: 2021-02-25T21:00:28-05:00
draft: false
author: wesj
---

# Directory of Apps

Publicly-exposed applications:
* This site - https://wesj.org
* Miniflux RSS Reader - https://miniflux.wesj.app
* Docker Registry - https://docker-registry.wesj.app
* PlantUML - https://plantuml.wesj.app

# Deployment Spec
The deployment spec for this cluster can be found [on Gitlab.](https://gitlab.com/wesj_org/wesj-k3s-argo)
