BASEURL_DEV=http://localhost:8080
BASEURL_PROD=https://wesj.org

server:
	hugo server -D
	
draft:
	hugo -D -b ${BASEURL_DEV}

blog:
	hugo -b ${BASEURL_PROD}